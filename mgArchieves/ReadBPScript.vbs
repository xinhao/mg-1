' ### This is the first version
' It uses ComponentFactory to get a list
' Then load each script to QTP
' Then Save the script to file system

Function ReadScript(strQCPath)
   Set qtApp = CreateObject("QuickTest.Application")
    qtApp.Launch
	qtApp.Visible = True
	If Not qtApp.TDConnection.IsConnected Then
		MsgBox "Connect to QC please"
	End If
	qtApp.OpenBusinessComponent strQCPath, False, False
	Set strMyComp = qtApp.BusinessComponent
	Set strMyAct = strMyComp.Actions.Item(1)
	strScript = strMyAct.GetScript
	ReadScript = strScript
End Function

Function WriteScript(strMyScript, strLocalPath)
	Const ForReading = 1, ForWriting = 2, ForAppending = 8
	Const NewFileCreated = True, NewFileNotCreated = False
	Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
	'strMyScriptLines = Split(strMyScript, vbCrLf) ' ### not necessary, can just write the whole string
	Set fso=createobject("Scripting.FileSystemObject")
	'Set qfile=fso.CreateTextFile(strLocalPath,True, True) 'Overwrite- True:can be overwritten, False:can't be overwritten; Unicode- True:Unicode, False: ASCII 
    ' not necessary to create the file first, can use OpenTextFile with create switch set to True
	' Test Unicode 试一下含中文能不能完成写操作
	'Output --> New File is created
	'Open the file in writing mode.
	Set qfile=fso.OpenTextFile(strLocalPath, ForWriting, NewFileCreated, TristateTrue) ' iomode, create, format: -1:Unicode, 0:ASCII, -2: system default
	REM For Each strLine in strMyScriptLines ' ### not necessary, can just write the whole string
		REM qfile.Writeline strLine
		REM 'reporter.ReportNote strLine
	REM Next
	'write contents to the file into a single line
    qfile.Write strMyScript
	qfile.Close
	'Release the allocated objects
	Set qfile=nothing
	Set fso=nothing
End Function

Function GetComponentList(strQCPath)
    Set qtApp = CreateObject("QuickTest.Application")
    qtApp.Launch
	qtApp.Visible = True
	If Not qtApp.TDConnection.IsConnected Then
		MsgBox "Connect QTP to QC then try again..."
	End If
	'strQCPath = "Components\MIB\MG"
	Set objFilter = qtApp.TDConnection.TDOTA.ComponentFolderFactory.FolderByPath(strQCPath).ComponentFactory.Filter
	objFilter.Filter("CO_NAME") = "Not(""* X"")"
	Set objComponentList = qtApp.TDConnection.TDOTA.ComponentFolderFactory.FolderByPath(strQCPath).ComponentFactory.NewList(objFilter.Text) ' ### all components under the folder strQCPath with filter not("* X")
	intCount = objComponentList.Count ' component count under the folder
	Dim ComponentList()
	ReDim ComponentList(intCount)
'	strDisplay = ""
'	For i = 1 to intCount
'		strDisplay = strDisplay & i & " -> " & objFilter.Fields.Item(i) & vbCrLf
'	Next
'	Reporter.ReportNote strDisplay
	For i = 1 to intCount
		strName = objComponentList.Item(i).Name ' each component's name
		ComponentList(i) = strName
	Next
    'Wait 1
	' ### hard code the list for now
	REM ComponentList(0) = "Login as admin"
	REM ComponentList(1) = "Create a load"
	REM ComponentList(2) = "Find the load"
	REM ComponentList(3) = "Cover - Assign Customer Rate"
	REM ComponentList(4) = "Cover - Accept Quote"
	REM ComponentList(5) = "Cover - Set Appointments"
	REM ComponentList(6) = "Cover - Assign Carrier Rate"
	REM ComponentList(7) = "Cover - Send Carrier Confirmation"
	REM ComponentList(8) = "Cover - Cover"
	REM ComponentList(9) = "Cover - Dispatch"
	REM ComponentList(10) = "Cover - Set ArrivalDeparture"
	REM ComponentList(11) = "CheckWebElement"
	REM ComponentList(12) = "Verify menubar"
	REM ComponentList(13) = "trials"
	GetComponentList = ComponentList
End Function

Dim arrList
arrList = GetComponentList("Components\MIB\MG")
For i = 1 to UBound(arrList)
    strMyScript = ReadScript("[QualityCenter] Components\MIB\MG\" & arrList(i))
    WriteScript strMyScript, "H:\mg\" & arrList(i) & ".vbs"
Next
