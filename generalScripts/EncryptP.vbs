' ====================================
' Author : Hao Xin
' Created Date: 10/29/2014

' Returns an encrypted string
'
' SYNTAX:
' EncryptP string [key]
'
' string: the plain text string to be encrypted
' key: optional encryption key
' ======================================
Dim temp, key
Const intL = 32, intU = 126

Sub Commented
temp = "plaintext"
key = "encryption key"

temp = Encrypt(temp,key)
WScript.Echo temp
temp1 = "*cdXZdm"
temp = Decrypt(temp,key)
WScript.Echo temp
WScript.Echo Decrypt(temp1,key)

'wscript.echo ModShift(170, 32, 127)
End Sub

Function ModShift(intNumber)
    ModShift = ((intNumber - intL) mod (intU - intL)) + intL
    Do While ModShift < intL
        ModShift = ModShift + intU -intL
    Loop
End Function

Function Encrypt(str, key)
 Dim lenKey, KeyPos, LenStr, x, Newstr
 
 Newstr = ""
 lenKey = Len(key)
 KeyPos = 1
 LenStr = Len(Str)
 str = StrReverse(str)
 For x = 1 To LenStr
      'WScript.Echo ModShift(asc(Mid(str,x,1)) + Asc(Mid(key,KeyPos,1)))
      Newstr = Newstr & chr(ModShift(asc(Mid(str,x,1)) + Asc(Mid(key,KeyPos,1))))
      KeyPos = keypos+1
      If KeyPos > lenKey Then KeyPos = 1
 Next
 encrypt = Newstr
End Function

' This is the decrypt function used in other scripts
Function Decrypt(str,key)
 Dim lenKey, KeyPos, LenStr, x, Newstr
 
 Newstr = ""
 lenKey = Len(key)
 KeyPos = 1
 LenStr = Len(Str)
 
 str=StrReverse(str)
 For x = LenStr To 1 Step -1
      '  WScript.Echo ModShift(asc(Mid(str,x,1)) - Asc(Mid(key,KeyPos,1)))
      Newstr = Newstr & chr(ModShift(asc(Mid(str,x,1)) - Asc(Mid(key,KeyPos,1))))
      KeyPos = KeyPos+1
      If KeyPos > lenKey Then KeyPos = 1
      Next
      Newstr=StrReverse(Newstr)
      Decrypt = Newstr
End Function

'Call Commented

Set objArgs = WScript.Arguments
'WScript.Echo objArgs.Count
blnHelp = False
If objArgs.Count = 0 Then
    blnHelp = True
ElseIf  objArgs.Count > 0 and objArgs.Item(0) = "/h" Then
    blnHelp = True
End If
If blnHelp Then
    WScript.Echo "Encrypt a string" & vbCrlf & vbCrlf & "SYNTAX:" & vbCrlf & _
    "EncryptP string [key]"  & vbCrlf & vbCrlf & _
    "string: the plain text string to be encrypted" & vbCrlf & "key: optional encryption key"
    WScript.Quit
End If
'WScript.Echo objArgs.Count
REM For I = 0 to objArgs.Count - 1
   REM WScript.Echo objArgs(I)
REM Next
'WScript.Echo objArgs.Item(1)
temp = objArgs.Item(0)
If objArgs.Count > 1 Then
    key = objArgs.Item(1)
Else
    key = "encryption key"
End If
WScript.Echo "Your PlainText: """ & temp & """" & vbCrlf & _
             "Encrypted Text: """ & Encrypt(temp, key) & """" & vbCrlf & _
             "Encryption Key: """ & key & """" & vbCrlf

' Clipboard not working, need to download and register something, abandon this feature
REM Dim clip
REM Set clip = CreateObject("WshExtra.Clipboard")
REM clip.Copy(Encrypt(temp, key))