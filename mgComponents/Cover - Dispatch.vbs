﻿'
'Dim strURL, objWS, objPageMG
'Const strBrowser = "iexplore.exe"
Set objWS = CreateObject("WScript.Shell")
Set objPageMG = Browser("title:=TMS").Page("title:=TMS")
strCoverStep = "Dispatch"


' ### Look for Assign Carrier Rate with class='mg-bubble-title and htmltext='Assign Carrier Rate'
' ### Then look for its child and grandchild <a>/<img>
' ### msgbox objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[contains(concat(' ', normalize-space(@class), ' '), 'mg-bubble-title') and contains(text(), 'Assign Carrier Rate')]/a/img").GetROProperty("html id")
' ### xpath:=//div[@class='mg-bubble-title' and contains(text(), 'Cover')] ' can't use .= because the strings always have trailing spaces
objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Click
'/****
'wait 1

' ### Locate email field
Set objElement = Browser("title:=TMS").Page("title:=TMS").Frame("name:=winExtDispatch.*IFrame").WebEdit("xpath:=//input[@name='sDispatchName']")
objElement.WaitProperty "visible", True
strName = "Driver"
strPhone = "555-555-5555"
strCity = "Current City"
strState = "Current State"
strPostalCode = "Postal Code"
strDate = Date
strDate = Right("0" & DatePart("m",strDate), 2) _ 
	& "/" & Right("0" & DatePart("d",strDate), 2) _
	& "/" & DatePart("yyyy",strDate)
strTime = FormatDateTime(Time,4)
objElement.Set strName
With Browser("title:=TMS").Page("title:=TMS").Frame("name:=winExtDispatch.*IFrame")
	.WebEdit("xpath:=//input[@name='sDispatchPhone']").Set strPhone
	.WebEdit("xpath:=//input[@name='sCity']").Set strCity
	.WebEdit("xpath:=//input[@name='sState']").Set strState
	.WebEdit("xpath:=//input[@name='sPostalCode']").Set strPostalCode
	.WebEdit("xpath:=//input[@name='ideCompleted']").Set strDate
	.WebEdit("xpath:=//input[@name='iteCompleted']").Set strTime
End With

Set objElementSave = Browser("title:=TMS").Page("title:=TMS").Frame("name:=winExtDispatch.*IFrame").WebElement("xpath:=//input[normalize-space(@value)='Save']")
objElementSave.Click

'Sub Commented

Wait 1 ' ###  try similar to waitforloading later *************************************

' ### check if the EZ button disappears
CheckEZ strCoverStep, 60

' ### Check if the EZ Click icon disappears
Sub CheckEZ(strCoverStep, intTimeOut)
	'intTimeOut = 60
	Dim i, objPageMG
	Set objPageMG = Browser("name:=TMS").Page("title:=TMS")
	i = intTimeOut
	Do While (objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1)) and i >0
		Wait 1
		i = i -1
	Loop
	Reporter.ReportNote strCoverStep & " - Loop times for EZ icon to disappear: " & (intTimeOut - i)
	If objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1) Then
		Reporter.ReportEvent micWarning, strCoverStep, "The item is still showing up"
	Else
		Reporter.ReportEvent micPass, strCoverStep, "The item disappears as expected"
	End If
End Sub

'End Sub