﻿Function uniqueString(ByVal intLength)
   If Not IsNumeric(intLength) Then
	   intLength = 8
   End If
   If intLength < 1 or intLength > 32 Then
	   intLength = 32
   End If
	Set TypeLib = CreateObject("Scriptlet.TypeLib")
	strGuid = TypeLib.Guid
	strGuid = Replace(strGuid, "-", "")
	uniqueString = Mid(strGuid, 34-intLength, intLength)
End Function

'Sub Commented
'
'Dim strURL, objWS, objPageMG
'Const strBrowser = "iexplore.exe"
Set objWS = CreateObject("WScript.Shell")
Set objPageMG = Browser("title:=TMS").Page("title:=TMS")
strCoverStep = "Send Carrier Confirmation"

'Sub ReportStrComp(strExp, strAct, strDes)
'	If StrComp(strAct, strExp, 1) = 0Then
'		Reporter.Reportevent micPass, strDes,"checkpoint passed"
'	Else
'		Reporter.ReportEvent micFail, strDes, "checkpoint failed" & vbNewLine & "Expected: [" & strExp & "] Actual: [" & strAct & "]"
'	End If
'End Sub
'
' ### After  Set Appointments
' ### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'objPageMG.Sync
' ###Assign Carrier Rate

' ### Look for Assign Carrier Rate with class='mg-bubble-title and htmltext='Assign Carrier Rate'
' ### Then look for its child and grandchild <a>/<img>
' ### msgbox objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[contains(concat(' ', normalize-space(@class), ' '), 'mg-bubble-title') and contains(text(), 'Assign Carrier Rate')]/a/img").GetROProperty("html id")
objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[contains(concat(' ', normalize-space(@class), ' '), 'mg-bubble-title') and contains(text(), '" & strCoverStep & "')]/a/img").Click
'/****
'If  objPageMG.Frame("name:=Detail").WebElement("xpath:=//td[.='" & strLoadNumber & "']/following-sibling::*[2]").GetROProperty("innertext") = "Pending" _ 
'	And Not objPageMG.Frame("name:=Detail").WebElement("html tag:=SPAN", "innertext:=" & strLoadNumber & ": To-Do List").Exist Then
'	' 1. look for the loadnumber element, it's a <td> element
'	' 2. look for immediate preceding sibling of (1), it's also a <td> element
'	' 3. look for child of (2), it's a <div> element and it's the only child of (2)
'	' Click the plus sign to expend, or it might be a minus sign if already expanded
'	objPageMG.Frame("name:=Detail").WebElement("xpath:=//td[.='" & strLoadNumber & "']/preceding-sibling::*[1]/div").Click
'End If


'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//*[@id='mgreportcombo-1544-inputEl']").Set "Rate"
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//input[starts-with(@id, 'mgreportcombo')]").Set "Rate" ' matches multiple
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//td[contains(@class,'x-trigger-cell') and contains(@class, 'x-unselectable')]/div").Click ' matches more
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//*[starts-with(@class,'x-trigger-cell')]").Click 
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//table/tbody/tr/td/table/tbody/tr/td[contains(@class,'x-trigger-cell') and contains(@class, 'x-unselectable')]/div[@role='button' and @id='ext-gen2267']").Click ' **works but dynamic id**
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/div/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td[contains(@class,'x-trigger-cell') and contains(@class, 'x-unselectable')]/div[@role='button']").Click ' match multiple
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//*[.='Rating']/../../../../div/div/div/div/div/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td[contains(@class,'x-trigger-cell') and contains(@class, 'x-unselectable')]/div[@role='button']").Click 'located wrong to the bottom dropdown button
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/span[.='Rating']").Click
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//div/span[.='Rating']/../../../div/span/div/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td/input[starts-with(@id,'mgreportcombo')]").Set "Rate"
'msgbox Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/span[.='Rating']/../../..").getroproperty("html id")
'*****/
'wait 2

' ### Locate email field
'Set objElementEmailTo = Browser("title:=TMS").Page("title:=TMS").Frame("name:=winExtTender.*IFrame").WebEdit("xpath:=//input[@name='sEmailTo']")
' ### name attribute was changed from "sEmailTo" to "email" for no obvious reason, found out on 10/28/2014
' ### the name on this screen was designed good - all have the same format: sCamelCase, why change sEmailTo  to email?
Set objElementEmailTo = Browser("title:=TMS").Page("title:=TMS").Frame("name:=winExtTender.*IFrame").WebEdit("xpath:=//input[@name='email']")
objElementEmailTo.WaitProperty "visible", True
strEmailTo = "hao_xin@jbhunt.com"
objElementEmailTo.Set strEmailTo

Set objElementTenderComments = Browser("title:=TMS").Page("title:=TMS").Frame("name:=winExtTender.*IFrame").WebEdit("xpath:=//textarea[@name='sTenderComments']")
strTenderComments = "This is an automation test ... " & uniqueString(12)
objElementTenderComments.Set strTenderComments

Set objElementSend = Browser("title:=TMS").Page("title:=TMS").Frame("name:=winExtTender.*IFrame").WebElement("xpath:=//input[@value='Send']")
objElementSend.Click
'End Sub

' ### Click OK button on the dialog
Set objOKButton = Browser("title:=TMS").Dialog("regexpwndtitle:=Message from webpage").WinButton("regexpwndtitle:=OK")
objOKButton.WaitProperty "visible", True, 20
objOKbutton.Click

' ### check if the EZ button disappears
CheckEZ strCoverStep, 60

' ### Check if the EZ Click icon disappears
Sub CheckEZ(strCoverStep, intTimeOut)
	'intTimeOut = 60
	Dim i, objPageMG
	Set objPageMG = Browser("name:=TMS").Page("title:=TMS")
	i = intTimeOut
	Do While (objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1)) and i >0
		Wait 1
		i = i -1
	Loop
	Reporter.ReportNote strCoverStep & " - Loop times for EZ icon to disappear: " & (intTimeOut - i)
	If objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1) Then
		Reporter.ReportEvent micWarning, strCoverStep, "The item is still showing up"
	Else
		Reporter.ReportEvent micPass, strCoverStep, "The item disappears as expected"
	End If
End Sub
