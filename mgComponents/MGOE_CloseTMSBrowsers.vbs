﻿Set objSessionExpiredOK = Browser("name:=TMS Login").Dialog("regexpwndtitle:=Message from webpage").WinButton("regexpwndtitle:=OK")
If objSessionExpiredOK.Exist(0) Then
	objSessionExpiredOK.Click
	Wait 1
End If

Dim strName, strCreationTime, strMask
strMask = "TMS"
intCreationTime = 0
Do While Browser("CreationTime:=" & intCreationTime).Exist(0)
	strName=Browser("CreationTime:="& intCreationTime).GetROProperty("name")
	'print strName & " " & intCreationTime & " " & strName & " " & Time
	If InStr(strName, strMask) > 0 Then
	'Close the current browser
		Browser("CreationTime:=" & strCreationTime).Close
	Else
		intCreationTime = intCreationTime + 1
	End If
Loop
