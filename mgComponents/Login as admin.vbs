﻿Dim strURL, WShell, intVertical, intHorizontal

Const strBroApp = "iexplore.exe"
'Const strBroApp = "chrome.exe"
strURL = "https://jbhunt2.mercurygate.net/MercuryGate/login/mgLogin.jsp"

Set WShell = CreateObject("WScript.Shell")

Set objSessionExpiredOK = Browser("name:=TMS Login").Dialog("regexpwndtitle:=Message from webpage").WinButton("regexpwndtitle:=OK")
If objSessionExpiredOK.Exist(0) Then
	objSessionExpiredOK.Click
	Wait 1
End If

Set objTMS = Browser("name:=TMS").Page("title:=TMS")
Set objLogin = Browser("name:=TMS Login").Page("title:=TMS Login")
If Not objTMS.Exist(0) Then
	If Not objLogin.Exist(0)  Then
		SystemUtil.Run strBroApp, strURL,,,3
	End If
	If strBroApp = "iexplore.exe" Then
		objLogin.Sync
'		objLogin.WebEdit("css:=#UserId").Set ""
'		objLogin.WebEdit("css:=#UserId").Click
'		Wshell.SendKeys("JISQHX3")
'		objLogin.WebEdit("css:=#Password").Set ""
'		objLogin.WebEdit("css:=#Password").Click
'		Wshell.SendKeys("mg123")
'		objLogin.WebElement("css:=input.InputText[name='submitbutton']").Click
		objLogin.WebEdit("html id:=UserId").Set "JISQHX3"
		objLogin.WebEdit("html id:=Password").Set "mg123"
        objLogin.WebCheckBox("name:=RememberMe").Set "Off"
		objLogin.WebButton("name:=    Sign In    ").Click
		objTMS.Sync
	Elseif strBroApp = "chrome.exe" Then ' not able to recognize page elements
		If Browser("title:=TMS Login").Exist Then
			strVersion = Browser("title:=TMS Login").GetROProperty("version")
			If InStr(1, strVersion, "Chrome", 1) <> 0 Then
				Reporter.ReportNote strVersion
			Else
				Reporter.ReportNote "Chrome not identified 1"
			End If
		Else
			Reporter.ReportEvent micDone, "Chrome",  "Chrome not identified 2"
			ExitTest
		End If
	End If
End If

' ### arrange the maximized TMS window and QTP window itself or debugging purpose
Sub ResizeWindow(strTitle,intWidth, intHeight, intLeft, intTop)
	Dim hwnd
	hwnd = Browser(strTitle).Object.HWND
	Window("hwnd:=" & hwnd).Resize intWidth, intHeight
	'Window("hwnd:=" & hwnd).Move intLeft, intTop
End Sub

Sub ResizeQTP(intWidth, intHeight, intLeft, intTop)
   Set objQTP = Window("title:=QuickTest Professional.*")
	objQTP.Resize intWidth, intHeight
	objQTP.Move intLeft, intTop
End Sub

Sub TheseAreNotRealSize 'read from registry
	Set objWMIService = GetObject("Winmgmts:\\.\root\cimv2")
	Set colItems = objWMIService.ExecQuery("Select * From Win32_DesktopMonitor where DeviceID = 'DesktopMonitor1'",,0)
	For Each objItem in colItems
		intHorizontal = objItem.ScreenWidth	' these are not actual screen size on VM
		intVertical = objItem.ScreenHeight
		'msgbox intHorizontal & " " & intVertical
	Next 
End Sub

Sub TheseAreRealButTotalSizeForMultipleMonitors
' ### Total screen width length
	intHorizontal = Window("regexpwndtitle:=Program Manager").GetROProperty("width") ' or text for regexpendtitle
	intVertical = Window("regexpwndtitle:=Program Manager").GetROProperty("height")
End Sub

Sub CommentedResize

	' # minimize the Launcher window
	hwnd = Browser("name:=TMS Launcher").Object.HWND
	Window("hwnd:=" & hwnd).Minimize
	
	strTitle = "title:=TMS"
	
	intTrayWidth = Window("object class:=Shell_TrayWnd").GetROProperty("width")
	intTrayHeight = Window("object class:=Shell_TrayWnd").GetROProperty("height")
	'msgbox intTrayWidth & " " & intTrayHeight
	TheseAreRealButTotalSizeForMultipleMonitors
	Reporter.ReportNote "H: " & intHorizontal & " V: " & intVertical & " W: " & intTrayWidth & " H: " & intTrayHeight
	a = intVertical - intTrayHeight
	b = intHorizontal/2
	Reporter.ReportNote "intHorizontal/2: " & b & " intVertical - intTrayHeight: " & a
	
	ResizeWindow strTitle, intHorizontal*0.47, intVertical - intTrayHeight, 0, 0
	'ResizeQTP  intHorizontal*0.47, intVertical - intTrayHeight, intHorizontal*0.47+1, 0
	wait 1 
End Sub
