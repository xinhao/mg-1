' ### Version 2 needs to open QTP but doesn't need to load each scripts to QTP,
' It uses ComponentFactory ExtendedStorage to load script from QC to local drive then copy and rename to target folder

Function SyncCopyScript(strQCPath, strLocalPath)
   Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set qtApp = CreateObject("QuickTest.Application")
    qtApp.Launch
	qtApp.Visible = True
	If Not qtApp.TDConnection.IsConnected Then
		MsgBox "Connect QTP to QC then try again..."
	End If
	'strQCPath = "Components\MIB\MG"
	Set objFilter = qtApp.TDConnection.TDOTA.ComponentFolderFactory.FolderByPath(strQCPath).ComponentFactory.Filter
	objFilter.Filter("CO_NAME") = "Not(""* X"")"
	Set objComponentList = qtApp.TDConnection.TDOTA.ComponentFolderFactory.FolderByPath(strQCPath).ComponentFactory.NewList(objFilter.Text) ' ### all components under the folder strQCPath with filter not("* X")
	intCount = objComponentList.Count ' component count under the folder
'	Dim ComponentList()
'	ReDim ComponentList(intCount)
'	strDisplay = ""
'	For i = 1 to intCount
'		strDisplay = strDisplay & i & " -> " & objFilter.Fields.Item(i) & vbCrLf
'	Next
'	Reporter.ReportNote strDisplay
	For i = 1 to intCount
		strName = objComponentList.Item(i).Name ' each component's name
		'ComponentList(i) = strName
        Set objES = objComponentList.Item(i).ExtendedStorage(0)
        strClientPath = objES.ClientPath
        objES.Load "", True ' Load and sync the ExtendedStorage folder before copy the script
        objFSO.CopyFile strClientPath & "\Action1\Script.mts", strLocalPath & strName & ".vbs"
	Next
	SyncCopyScript = intCount
End Function

strQCPath = "Components\MIB\MG"
strLocalPath = "H:\mg\components"
SyncCopyScript strQCPath, strLocalPath
