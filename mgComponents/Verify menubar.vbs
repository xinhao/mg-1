﻿' This verification only applies when the elements exist, it won't apply when the elements are out of screen
' e.g. File menu x=1, y=-175 which is out of the screen

Set objPageMG = Browser("title:=TMS").Page("title:=TMS")

'verify menubar File menu
If objPageMG.WebElement("html id:=dm0m0i0tdT", "innertextl:=File").Exist Then
	Reporter.ReportEvent micPass, "Verify menubar", "File menu checkpoint passed"
'	If objPageMG.WebElement("html id:=dm0m0i0tdT", "innertextl:=File").GetROProperty("y") < 0 Then
'		Reporter.ReportEvent micFail, "Verify menubar position", "Failed - menubar is out of screen"
'	End If
Else
	Reporter.ReportEvent micFail, "Verify menubar", "File menu checkpoint failed"
End If

'verify toolbar
If objPageMG.Image("name:=Image" , "file name:=SelectCompany16.png").Exist Then
	Reporter.ReportEvent micPass, "Verify toolbar", "Company toolbar button image checkpoint passed"
Else
	Reporter.ReportEvent micFail, "Verify toolbar", "Company toolbar button image checkpoint failed"
End If

'Verify banner
If objPageMG.Image("html id:=logo").Exist Then
	Reporter.ReportEvent micPass, "Verify banner", "Company logo checkpoint passed"
Else
	Reporter.ReportEvent micFail, "Verify banner", "Company logo checkpoint failed"
End If

' Add comments to verify ExtendedStorage(0).Load   test v3
